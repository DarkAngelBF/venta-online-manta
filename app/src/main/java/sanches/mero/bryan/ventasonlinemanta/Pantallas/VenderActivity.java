package sanches.mero.bryan.ventasonlinemanta.Pantallas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import sanches.mero.bryan.ventasonlinemanta.Fragmentos.AuditoriaFragment;
import sanches.mero.bryan.ventasonlinemanta.Fragmentos.HogarFragment;
import sanches.mero.bryan.ventasonlinemanta.Fragmentos.TecnologiaFragment;
import sanches.mero.bryan.ventasonlinemanta.Fragmentos.VestimentaFragment;
import sanches.mero.bryan.ventasonlinemanta.MainActivity;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Usuarios;
import sanches.mero.bryan.ventasonlinemanta.R;

public class VenderActivity extends AppCompatActivity implements View.OnClickListener{

    private BottomNavigationView bottomNavigationView;
    private EditText vender;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private CircleImageView circleImageView;
    private DatabaseReference usuario;
    private String userId;
    private TextView textView;
    private LinearLayout linearLayout;
    private ImageView imageView;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vender);

        vender = (EditText)findViewById(R.id.TXTIrAVender);
        circleImageView = (CircleImageView)findViewById(R.id.IMGPerfil);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();
        textView = (TextView)findViewById(R.id.TXTName);
        linearLayout = (LinearLayout)findViewById(R.id.Linear);
        imageView = (ImageView)findViewById(R.id.IMGCerrar);
        imageView.setOnClickListener(this);
        vender.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        Botton();
        Perfil();
    }

    private void Perfil() {
        usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Log.e("LOG", snapshot.toString());
                    String key = usuario.child("USUARIOS").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuarios usuario_perfil = snapshot.getValue(Usuarios.class);
                        Picasso.get().load(usuario_perfil.getFoto()).into(circleImageView);
                    }else {
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void Botton(){
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);

        int[] image = {R.drawable.ic_important_devices_black_24dp, R.drawable.ic_weekend_black_24dp,
                R.drawable.ic_watch_black_24dp, R.drawable.ic_shopping_cart_black_24dp};
        int[] color = {ContextCompat.getColor(this, R.color.firstColor), ContextCompat.getColor(this, R.color.secondColor),
                ContextCompat.getColor(this, R.color.thirdColor), ContextCompat.getColor(this, R.color.fourthColor)};

        if (bottomNavigationView != null) {
            bottomNavigationView.isWithText(false);
            bottomNavigationView.isColoredBackground(false);
            bottomNavigationView.setTextActiveSize(getResources().getDimension(R.dimen.text_active));
            bottomNavigationView.setTextInactiveSize(getResources().getDimension(R.dimen.text_inactive));
            bottomNavigationView.setItemActiveColorWithoutColoredBackground(ContextCompat.getColor(this, R.color.blanco));
        }

        BottomNavigationItem bottomNavigationItem = new BottomNavigationItem
                (getString(R.string.tecno), color[0], image[0]);
        BottomNavigationItem bottomNavigationItem1 = new BottomNavigationItem
                (getString(R.string.electro), color[1], image[1]);
        BottomNavigationItem bottomNavigationItem2 = new BottomNavigationItem
                (getString(R.string.ropa), color[2], image[2]);
        BottomNavigationItem bottomNavigationItem3 = new BottomNavigationItem
                (getString(R.string.informe), color[3], image[3]);


        bottomNavigationView.addTab(bottomNavigationItem);
        bottomNavigationView.addTab(bottomNavigationItem1);
        bottomNavigationView.addTab(bottomNavigationItem2);
        bottomNavigationView.addTab(bottomNavigationItem3);

        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new TecnologiaFragment()).commit();

        bottomNavigationView.setOnBottomNavigationItemClickListener(new OnBottomNavigationItemClickListener() {
            @Override
            public void onNavigationItemClick(int index) {
                switch (index){

                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new TecnologiaFragment()).commit();
                        vender.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new HogarFragment()).commit();
                        vender.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new VestimentaFragment()).commit();
                        vender.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.GONE);
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new AuditoriaFragment()).commit();
                        vender.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.TXTIrAVender:
                startActivity(new Intent(this, PublicarProductoActivity.class));
                break;

            case R.id.IMGPerfil:
                startActivity(new Intent(this, PerfilActivity.class));
                break;
            case R.id.IMGCerrar:
                firebaseAuth.signOut();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
        }

    }


}
