package sanches.mero.bryan.ventasonlinemanta.Modelos;

public class Producto {
    private String NombreP, PrecioP, FotoP, NombrePersona, FotoPersona, DetalleP, IDPublicante, Estado, Telefono, idComprador;

    public Producto() {
    }

    public Producto(String nombreP, String precioP, String fotoP, String nombrePersona, String fotoPersona, String detalleP, String IDPublicante, String estado, String telefono, String idComprador) {
        NombreP = nombreP;
        PrecioP = precioP;
        FotoP = fotoP;
        NombrePersona = nombrePersona;
        FotoPersona = fotoPersona;
        DetalleP = detalleP;
        this.IDPublicante = IDPublicante;
        Estado = estado;
        Telefono = telefono;
        this.idComprador = idComprador;
    }

    public String getNombreP() {
        return NombreP;
    }

    public void setNombreP(String nombreP) {
        NombreP = nombreP;
    }

    public String getPrecioP() {
        return PrecioP;
    }

    public void setPrecioP(String precioP) {
        PrecioP = precioP;
    }

    public String getFotoP() {
        return FotoP;
    }

    public void setFotoP(String fotoP) {
        FotoP = fotoP;
    }

    public String getNombrePersona() {
        return NombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        NombrePersona = nombrePersona;
    }

    public String getFotoPersona() {
        return FotoPersona;
    }

    public void setFotoPersona(String fotoPersona) {
        FotoPersona = fotoPersona;
    }

    public String getDetalleP() {
        return DetalleP;
    }

    public void setDetalleP(String detalleP) {
        DetalleP = detalleP;
    }

    public String getIDPublicante() {
        return IDPublicante;
    }

    public void setIDPublicante(String IDPublicante) {
        this.IDPublicante = IDPublicante;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(String idComprador) {
        this.idComprador = idComprador;
    }
}