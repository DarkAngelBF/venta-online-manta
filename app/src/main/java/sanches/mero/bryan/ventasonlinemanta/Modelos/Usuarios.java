package sanches.mero.bryan.ventasonlinemanta.Modelos;

public class Usuarios {

    private String Nombres, Apellidos, Cedula, Direccion, Celular, Correo, Foto;

    public Usuarios() {

    }

    public Usuarios(String nombres, String apellidos, String cedula, String direccion, String celular, String correo, String foto) {
        Nombres = nombres;
        Apellidos = apellidos;
        Cedula = cedula;
        Direccion = direccion;
        Celular = celular;
        Correo = correo;
        Foto = foto;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }
}
