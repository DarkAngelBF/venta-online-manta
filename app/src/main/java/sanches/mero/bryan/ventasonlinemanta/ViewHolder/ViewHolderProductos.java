package sanches.mero.bryan.ventasonlinemanta.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;
import sanches.mero.bryan.ventasonlinemanta.Interfaces.ItemClickListener;
import sanches.mero.bryan.ventasonlinemanta.R;

public class ViewHolderProductos extends RecyclerView.ViewHolder implements View.OnClickListener{


    private ItemClickListener itemClickListener;
    public TextView nombrePersona, nombreProducto, PrecioProducto;
    public CircleImageView circleImageViewPersona, imageViewProducto;

    public ViewHolderProductos(@NonNull View itemView) {
        super(itemView);
        nombrePersona = (TextView)itemView.findViewById(R.id.LBLNombrePersona);
        nombreProducto = (TextView)itemView.findViewById(R.id.LBLNombreProducto);
        PrecioProducto = (TextView)itemView.findViewById(R.id.LBLPrecioProducto);
        circleImageViewPersona = (CircleImageView)itemView.findViewById(R.id.CircleFotoPersona);
        imageViewProducto = (CircleImageView) itemView.findViewById(R.id.ImagenViewProducto);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
