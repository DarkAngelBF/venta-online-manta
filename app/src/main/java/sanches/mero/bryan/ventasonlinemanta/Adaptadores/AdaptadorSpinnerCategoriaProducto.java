package sanches.mero.bryan.ventasonlinemanta.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import sanches.mero.bryan.ventasonlinemanta.Modelos.SpinnerCategoriaProducto;
import sanches.mero.bryan.ventasonlinemanta.R;

public class AdaptadorSpinnerCategoriaProducto extends ArrayAdapter<SpinnerCategoriaProducto> {


    public AdaptadorSpinnerCategoriaProducto(@NonNull Context context, ArrayList<SpinnerCategoriaProducto> NumberList) {
        super(context, 0, NumberList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.layout_spinner, parent, false
            );
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.IMGSP);
        TextView textView = (TextView) convertView.findViewById(R.id.LBLSP);

        SpinnerCategoriaProducto spinnerCategoriaProducto = getItem(position);

        if (spinnerCategoriaProducto != null) {
            imageView.setImageResource(spinnerCategoriaProducto.getLogo());
            textView.setText(spinnerCategoriaProducto.getNombre());
        }
        return convertView;
    }
}
