package sanches.mero.bryan.ventasonlinemanta.Fragmentos;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import sanches.mero.bryan.ventasonlinemanta.Interfaces.ItemClickListener;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Producto;
import sanches.mero.bryan.ventasonlinemanta.Pantallas.DetalleActivity;
import sanches.mero.bryan.ventasonlinemanta.R;
import sanches.mero.bryan.ventasonlinemanta.ViewHolder.ViewHolderProductos;


public class AuditoriaFragment extends Fragment {

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference tecnologia;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseRecyclerAdapter<Producto, ViewHolderProductos> recyclerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auditoria, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.RecyclerViewAudi);
        firebaseDatabase = FirebaseDatabase.getInstance();
        tecnologia = firebaseDatabase.getReference().child("USUARIOS").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("HISTORIAL");
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        Auditoria();
        return view;
    }

    private void Auditoria() {

        recyclerAdapter = new FirebaseRecyclerAdapter<Producto, ViewHolderProductos>(Producto.class, R.layout.layout_producto,
                ViewHolderProductos.class, tecnologia) {
            @Override
            protected void populateViewHolder(final ViewHolderProductos viewHolderProductos, final Producto producto, int i) {
                viewHolderProductos.PrecioProducto.setText(producto.getEstado());
                viewHolderProductos.nombreProducto.setText(producto.getNombreP());
                viewHolderProductos.nombrePersona.setText(producto.getNombrePersona());
                Picasso.get().load(producto.getFotoP()).into(viewHolderProductos.imageViewProducto);
                Picasso.get().load(producto.getFotoPersona()).into(viewHolderProductos.circleImageViewPersona);
                viewHolderProductos.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                });
            }
        };
        recyclerView.setAdapter(recyclerAdapter);
    }
}
