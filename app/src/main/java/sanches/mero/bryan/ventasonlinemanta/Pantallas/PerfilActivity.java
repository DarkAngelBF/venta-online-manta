package sanches.mero.bryan.ventasonlinemanta.Pantallas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Usuarios;
import sanches.mero.bryan.ventasonlinemanta.R;

public class PerfilActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private CircleImageView perfil;
    private DatabaseReference usuario;
    private String userId;
    private TextView direccion, cedula, celular, correo, nombresApellidos;
    private Toolbar Nombres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);


        perfil = (CircleImageView)findViewById(R.id.IMGPerfilUser);
        direccion = (TextView)findViewById(R.id.LBLDireccionP);
        cedula = (TextView)findViewById(R.id.LBLCedulaP);
        celular = (TextView)findViewById(R.id.LBLTelefonoP);
        correo = (TextView)findViewById(R.id.LBLEmailP);
        Nombres = (Toolbar)findViewById(R.id.toolbarUsuario);
        nombresApellidos = (TextView)findViewById(R.id.LBLNombresApellidos);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();
        usuario = firebaseDatabase.getReference();

        Perfil();

    }

    private void Perfil() {
        usuario.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Log.e("LOG", snapshot.toString());
                    String key = usuario.child("USUARIOS").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){

                        Usuarios usuario_perfil = snapshot.getValue(Usuarios.class);
                        Picasso.get().load(usuario_perfil.getFoto()).into(perfil);
                        Nombres.setTitle(usuario_perfil.getNombres());
                        nombresApellidos.setText(usuario_perfil.getNombres() + " " + usuario_perfil.getApellidos());
                        direccion.setText(usuario_perfil.getDireccion());
                        cedula.setText(usuario_perfil.getCedula());
                        celular.setText(usuario_perfil.getCelular());
                        correo.setText(usuario_perfil.getCorreo());

                    }else {
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
