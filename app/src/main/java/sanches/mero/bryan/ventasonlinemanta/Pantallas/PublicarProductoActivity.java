package sanches.mero.bryan.ventasonlinemanta.Pantallas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.UUID;

import sanches.mero.bryan.ventasonlinemanta.Adaptadores.AdaptadorSpinnerCategoriaProducto;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Producto;
import sanches.mero.bryan.ventasonlinemanta.Modelos.SpinnerCategoriaProducto;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Usuarios;
import sanches.mero.bryan.ventasonlinemanta.R;

public class PublicarProductoActivity extends AppCompatActivity implements View.OnClickListener{

    private Spinner spinner;
    private ArrayList<SpinnerCategoriaProducto> spinnerCategoriaProductos;
    private AdaptadorSpinnerCategoriaProducto adaptadorSpinnerCategoriaProducto;
    private String Categoria, FotoP, NombrePersona, FotoPersona, Celular;
    private EditText NombreP, PrecioP, DetalleP;
    private ImageView imageView;
    private Uri uriFoto;
    private Button Foto, Publicar;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReferenceU, databaseReferenceP;
    private StorageReference storage;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicar_producto);

        ViewI();
        Firebase();
        Perfil();


    }

    private void ViewI() {

        progressDialog = new ProgressDialog(this);
        imageView = (ImageView)findViewById(R.id.IMGProducto);
        NombreP = (EditText)findViewById(R.id.TXTNombresP);
        PrecioP = (EditText)findViewById(R.id.TXTPrecioP);
        Foto = (Button)findViewById(R.id.BTNFotoP);
        Publicar = (Button)findViewById(R.id.BTNVenderProducto);
        spinner = (Spinner)findViewById(R.id.spinnerCategoria);
        DetalleP = (EditText)findViewById(R.id.TXTDetalleProducto);
        Foto.setOnClickListener(this);
        Publicar.setOnClickListener(this);
        SpinnerItem();
        adaptadorSpinnerCategoriaProducto = new AdaptadorSpinnerCategoriaProducto(this, spinnerCategoriaProductos);
        spinner.setAdapter(adaptadorSpinnerCategoriaProducto);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerCategoriaProducto spinnerCategoriaProducto = (SpinnerCategoriaProducto) parent.getItemAtPosition(position);
                Categoria = spinnerCategoriaProducto.getNombre();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void Firebase() {

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReferenceU = firebaseDatabase.getReference();
        databaseReferenceP = firebaseDatabase.getReference();
        storage = FirebaseStorage.getInstance().getReference().child("PRODUCTOS");
        progressDialog = new ProgressDialog(this);

    }

    private void SpinnerItem() {

        spinnerCategoriaProductos = new ArrayList<>();
        spinnerCategoriaProductos.add(new SpinnerCategoriaProducto(getString(R.string.elegir), R.drawable.ic_lista));
        spinnerCategoriaProductos.add(new SpinnerCategoriaProducto(getString(R.string.tecno), R.drawable.ic_important_devices_black_24dp));
        spinnerCategoriaProductos.add(new SpinnerCategoriaProducto(getString(R.string.electro), R.drawable.ic_weekend_black_24dp));
        spinnerCategoriaProductos.add(new SpinnerCategoriaProducto(getString(R.string.ropa), R.drawable.ic_watch_black_24dp));
    }

    private void Perfil() {
        databaseReferenceU.child("USUARIOS").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    Log.e("LOG", snapshot.toString());
                    String key = databaseReferenceU.child("USUARIOS").child(firebaseAuth.getCurrentUser().getUid()).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuarios usuario_perfil = snapshot.getValue(Usuarios.class);
                        NombrePersona = usuario_perfil.getNombres() + " " + usuario_perfil.getApellidos();
                        FotoPersona = usuario_perfil.getFoto();
                        Celular = usuario_perfil.getCelular();
                    }else {
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.BTNFotoP:

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);

                break;

            case R.id.BTNVenderProducto:

                if (NombreP.getText().toString().isEmpty()) {

                }
                if (PrecioP.getText().toString().isEmpty()) {

                }
                if (Categoria.equals(getString(R.string.elegir))) {

                }
                if (uriFoto == null) {

                }if (DetalleP.getText().toString().isEmpty()){

                }else {
                    SubirFoto();
                }
                break;

        }

    }

    private void SubirFoto() {
        progressDialog.setCancelable(false);
        progressDialog.setMessage("PUBLICANDO SU PRODUCTO");
        progressDialog.show();
        final StorageReference filePath = storage.child(uriFoto.getLastPathSegment());
        final UploadTask uploadTask = filePath.putFile(uriFoto);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return filePath.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    FotoP = downloadUri.toString();
                    PublicarProducto();
                } else {
                    //error
                }
            }
        });

    }

    private void PublicarProducto() {

        Producto producto = new Producto();
        producto.setFotoP(FotoP);
        producto.setFotoPersona(FotoPersona);
        producto.setNombreP(NombreP.getText().toString().trim());
        producto.setPrecioP(PrecioP.getText().toString().trim());
        producto.setNombrePersona(NombrePersona);
        producto.setDetalleP(DetalleP.getText().toString().trim());
        producto.setIDPublicante(firebaseAuth.getCurrentUser().getUid());
        producto.setEstado("0");
        producto.setTelefono(Celular);
        producto.setIdComprador("");
        databaseReferenceP.child(Categoria).child(UUID.randomUUID().toString()).setValue(producto)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.dismiss();
                        onBackPressed();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(PublicarProductoActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uriFoto = result.getUri();
                imageView.setImageURI(uriFoto);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
