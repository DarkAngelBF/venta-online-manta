package sanches.mero.bryan.ventasonlinemanta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import sanches.mero.bryan.ventasonlinemanta.Pantallas.RegistrarseActivity;
import sanches.mero.bryan.ventasonlinemanta.Pantallas.VenderActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button IniciarSesion, Registrarse;
    private EditText Correo, Clave;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IniciarSesion = (Button)findViewById(R.id.BTNLogin);
        Registrarse = (Button)findViewById(R.id.BTNRegistarseL);
        Correo = (EditText)findViewById(R.id.TXTCorreoL);
        Clave = (EditText)findViewById(R.id.TXTClaveL);
        firebaseAuth = FirebaseAuth.getInstance();

        IniciarSesion.setOnClickListener(this);
        Registrarse.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BTNLogin:

                progressDialog.setCancelable(false);
                progressDialog.show();
                firebaseAuth.signInWithEmailAndPassword(Correo.getText().toString().trim(), Clave.getText().toString().trim())
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    startActivity(new Intent(MainActivity.this, VenderActivity.class));
                                    finish();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(MainActivity.this, "Correo o contraseña incorrecto", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }).addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Error" + e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });


                break;

            case R.id.BTNRegistarseL:
                startActivity(new Intent(this, RegistrarseActivity.class));
                break;

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser!=null){
            startActivity(new Intent(MainActivity.this, VenderActivity.class));
            finish();
        }else {
            //necesita iniciar sesion
        }

    }
}
