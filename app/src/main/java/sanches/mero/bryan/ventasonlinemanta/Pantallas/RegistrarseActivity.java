package sanches.mero.bryan.ventasonlinemanta.Pantallas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;
import sanches.mero.bryan.ventasonlinemanta.MainActivity;
import sanches.mero.bryan.ventasonlinemanta.Modelos.Usuarios;
import sanches.mero.bryan.ventasonlinemanta.R;

public class RegistrarseActivity extends AppCompatActivity implements View.OnClickListener{

    private CircleImageView circleImageView;
    private EditText nombres, apellidos, cedula, direccion, telefono, correo, clave;
    private Button registrar;
    private Uri resultUri = null;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private StorageReference storage;
    private ProgressDialog progressDialog;
    private String urlFoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        circleImageView = (CircleImageView)findViewById(R.id.profile_image);
        nombres = (EditText)findViewById(R.id.TXTNombresR);
        apellidos = (EditText)findViewById(R.id.TXTApellidosR);
        cedula = (EditText)findViewById(R.id.TXTCedulaR);
        direccion = (EditText)findViewById(R.id.TXTDireccionR);
        telefono = (EditText)findViewById(R.id.TXTTelefonoR);
        correo = (EditText)findViewById(R.id.TXTCorreoR);
        clave = (EditText)findViewById(R.id.TXTClaveR);
        registrar = (Button) findViewById(R.id.BTNRegistarse);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("USUARIOS");
        storage = FirebaseStorage.getInstance().getReference().child("FOTOS DE PERFIL");
        progressDialog = new ProgressDialog(this);



        registrar.setOnClickListener(this);
        circleImageView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BTNRegistarse:

                if (nombres.getText().toString().isEmpty()){

                }if (apellidos.getText().toString().isEmpty()){

                }if (cedula.getText().toString().isEmpty()){

                }if (direccion.getText().toString().isEmpty()){

                }if (telefono.getText().toString().isEmpty()){

                }if (correo.getText().toString().isEmpty()){

                }if (clave.getText().toString().isEmpty()) {

                }if (resultUri == null){

                }else {
                    Registrarse();
                }


                break;

            case R.id.profile_image:

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);

                break;

        }

    }

    private void Registrarse() {
        progressDialog.setMessage("REGISTRANDO");
        progressDialog.setCancelable(false);
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(correo.getText().toString().trim(), clave.getText().toString().trim())
                                    .addOnCompleteListener(RegistrarseActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            SubirFoto();
                                        }

                                    }).addOnFailureListener(RegistrarseActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.hide();
                Toast.makeText(RegistrarseActivity.this, "ACA", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void SubirFoto() {

        final StorageReference filePath = storage.child(resultUri.getLastPathSegment());
        final UploadTask uploadTask = filePath.putFile(resultUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return filePath.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    urlFoto = downloadUri.toString();
                    SubirDatos();
                } else {
                    //error
                }
            }
        });

    }

    private void SubirDatos(){
        Usuarios usuarios = new Usuarios();
        usuarios.setNombres(nombres.getText().toString().trim());
        usuarios.setApellidos(apellidos.getText().toString().trim());
        usuarios.setCedula(cedula.getText().toString().trim());
        usuarios.setCelular(telefono.getText().toString().trim());
        usuarios.setCorreo(correo.getText().toString().trim());
        usuarios.setDireccion(direccion.getText().toString().trim());
        usuarios.setFoto(urlFoto);
        databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuarios)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        progressDialog.dismiss();
                        Toast.makeText(RegistrarseActivity.this, "Usuario Registrado", Toast.LENGTH_SHORT).show();
                        firebaseAuth.signOut();
                        startActivity(new Intent(RegistrarseActivity.this, MainActivity.class));
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(RegistrarseActivity.this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                circleImageView.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
