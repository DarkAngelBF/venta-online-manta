package sanches.mero.bryan.ventasonlinemanta.Pantallas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.codesgood.views.JustifiedTextView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.UUID;

import sanches.mero.bryan.ventasonlinemanta.Modelos.Producto;
import sanches.mero.bryan.ventasonlinemanta.R;

public class DetalleActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView NombrePersona, NombreProducto, PrecioProducto;
    private JustifiedTextView DetalleProducto;
    private Button Comprar, BajarPrecio, Actualizar, Vendido;
    private ImageView FotoProducto;
    private String PrecioNuevo, Categoria, KeyPersona, KeyProducto, Cell, idCliente, FotoP, FotoPro;
    int precioActual;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference ProductoReference, HistorialDueno, HistorialCliente;
    private Boolean datos;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        NombrePersona = (TextView)findViewById(R.id.LBLNombrePersonaD);
        NombreProducto = (TextView)findViewById(R.id.LBLNombreProductoD);
        PrecioProducto = (TextView)findViewById(R.id.LBLPrecioProductoD);
        DetalleProducto = (JustifiedTextView)findViewById(R.id.LBLDetalleP);
        FotoProducto = (ImageView) findViewById(R.id.ImagenProductoDetalle);
        Comprar = (Button)findViewById(R.id.BTNComprarP);
        BajarPrecio = (Button)findViewById(R.id.BTNBajarPrecio);
        Actualizar = (Button)findViewById(R.id.BTNGuardarCambios);
        Vendido = (Button)findViewById(R.id.BTNVendido);
        Vendido.setOnClickListener(this);
        Comprar.setOnClickListener(this);
        BajarPrecio.setOnClickListener(this);
        Actualizar.setOnClickListener(this);

        firebaseDatabase = FirebaseDatabase.getInstance();

        KeyPersona = getIntent().getStringExtra("idPersona");
        KeyProducto = getIntent().getStringExtra("id");
        Categoria = getIntent().getStringExtra(getString(R.string.categoria));

        ProductoReference = firebaseDatabase.getReference().child(Categoria);
        HistorialDueno = firebaseDatabase.getReference().child("USUARIOS");
        HistorialCliente = firebaseDatabase.getReference().child("USUARIOS");
        datos = true;
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId.equals(KeyPersona)){
            BajarPrecio.setVisibility(View.VISIBLE);
            Actualizar.setVisibility(View.VISIBLE);
        }else {
            Comprar.setVisibility(View.VISIBLE);
        }

        if (getIntent().getStringExtra("estado").equals("1")){
            Vendido.setVisibility(View.VISIBLE);
            BajarPrecio.setVisibility(View.GONE);
            Actualizar.setVisibility(View.GONE);
        }
        Datos();
    }

    private void Datos() {

        ProductoReference.child(KeyProducto).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (datos == true){
                    Producto producto = dataSnapshot.getValue(Producto.class);
                    Picasso.get().load(producto.getFotoP()).into(FotoProducto);
                    NombrePersona.setText(producto.getNombrePersona());
                    NombreProducto.setText(producto.getNombreP());
                    PrecioProducto.setText("$"+producto.getPrecioP());
                    DetalleProducto.setText(producto.getDetalleP());
                    precioActual = Integer.parseInt(producto.getPrecioP());
                    idCliente = producto.getIdComprador();
                    FotoP = producto.getFotoPersona();
                    FotoPro = producto.getFotoP();
                    String string = producto.getTelefono();
                    String sSubCadena = string.substring(1,10);
                    Cell = sSubCadena;
                }else {

                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BTNBajarPrecio:
                PrecioNuevo = String.valueOf(precioActual - 2);
                PrecioProducto.setText("$"+PrecioNuevo);
                precioActual = Integer.parseInt(PrecioNuevo);
                break;

            case R.id.BTNComprarP:
                HashMap<String, Object> HashMap = new HashMap<>();
                HashMap.put("idComprador", FirebaseAuth.getInstance().getCurrentUser().getUid());
                HashMap.put("estado", "1");
                ProductoReference.child(KeyProducto).updateChildren(HashMap);
                String msj = "Estoy Interesado en el artículo " + NombreProducto.getText().toString();
                String numeroTel = "+593" + Cell;
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String uri = "whatsapp://send?phone=" + numeroTel + "&text=" + msj;
                intent.setData(Uri.parse(uri));
                startActivity(intent);
                break;

            case R.id.BTNGuardarCambios:
                HashMap<String, Object> objectHashMap = new HashMap<>();
                objectHashMap.put("precioP", PrecioNuevo);
                ProductoReference.child(KeyProducto).updateChildren(objectHashMap);
                onBackPressed();
                break;

            case R.id.BTNVendido:
                datos = false;
                Producto p = new Producto();
                p.setNombrePersona(NombrePersona.getText().toString());
                p.setNombreP(NombreProducto.getText().toString());
                p.setFotoPersona(FotoP);
                p.setFotoP(FotoPro);
                p.setEstado("VENDIDO");
                HistorialDueno.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("HISTORIAL").child(UUID.randomUUID().toString()).setValue(p)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                HistorialClientee();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

                break;
        }
    }

    private void HistorialClientee() {

        Producto pe = new Producto();
        pe.setNombrePersona(NombrePersona.getText().toString());
        pe.setNombreP(NombreProducto.getText().toString());
        pe.setFotoPersona(FotoP);
        pe.setFotoP(FotoPro);
        pe.setEstado("COMPRADO");
        HistorialCliente.child(idCliente).child("HISTORIAL").child(UUID.randomUUID().toString()).setValue(pe)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Eliminar();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    private void Eliminar() {
        ProductoReference.child(KeyProducto).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
}
